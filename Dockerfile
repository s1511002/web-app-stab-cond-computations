FROM node:18-alpine3.15 as builder

# install dependencies
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci

COPY . .

RUN npm run build

ENV HOST=0.0.0.0
ENV PORT=3000
ENTRYPOINT ["node", "build"]
